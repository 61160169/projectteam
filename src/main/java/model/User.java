/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author DELL
 */
public class User {
    private int id;
    private int emp;
   
    private String username;
    private String password;

    public User(int id,String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }
    
   

    public User(int id, int emp,String username, String password) {
        this.id = id;
        this.emp = emp;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

   

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        
    }

    public int getEmp() {
        return emp;
    }

    public void setEmp(int emp) {
        this.emp = emp;
    }

  

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", emp=" + emp + ", username=" + username + ", password=" + password + '}';
    }

    
    

  

   

    
    
    
    
    

    
  
    
    
    
    
    
    
}
