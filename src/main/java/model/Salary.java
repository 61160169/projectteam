/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author erngeoey
 */
public class Salary {
    Employee emp;
    String payment;
    String accountNumber;
    int Salary;

    public Salary(Employee emp, String payment, String accountNumber, int Salary) {
        this.emp = emp;
        this.payment = payment;
        this.accountNumber = accountNumber;
        this.Salary = Salary;
    }

    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getSalary() {
        return Salary;
    }

    public void setSalary(int Salary) {
        this.Salary = Salary;
    }

    @Override
    public String toString() {
        return "Salary{" + "emp=" + emp + ", payment=" + payment + ", accountNumber=" + accountNumber + ", Salary=" + Salary + '}';
    }
    
    
}
