/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author User
 */
public class StockUsed {
   int id ;
    Stock stock ;
    Employee emp ;
    int amount ;

    public StockUsed(int id, Stock stock, Employee emp, int amount) {
        this.id = id;
        this.stock = stock;
        this.emp = emp;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public Stock getStock() {
        return stock;
    }

    public Employee getEmp() {
        return emp;
    }

    public int getAmount() {
        return amount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "StockUsed{" + "id=" + id + ", stock=" + stock + ", emp=" + emp + ", amount=" + amount + '}';
    }
     
}
