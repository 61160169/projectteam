/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NOOM HR
 */
public class Promotion {
    
    private int id;
    private String name;
    private String detail;
    private double discount;
    private String datestart;
    private String datestop; 

    public Promotion(int id, String name, String detail, double discount, String datestart, String datestop) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.discount = discount;
        this.datestart = datestart;
        this.datestop = datestop;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getDatestart() {
        return datestart;
    }

    public void setDatestart(String datestart) {
        this.datestart = datestart;
    }

    public String getDatestop() {
        return datestop;
    }

    public void setDatestop(String datestop) {
        this.datestop = datestop;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", detail=" + detail + ", discount=" + discount + ", datestart=" + datestart + ", datestop=" + datestop + '}';
    }

    
}
