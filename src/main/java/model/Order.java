/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NOOM HR
 */
public class Order {
    private int Id;
    private double Price;
    private double Discount;
    private double Receivemoney;
    private String Date;
    private double Total;
    private double Change;

    public Order(int Id, double Price, double Discount, double Receivemoney, String Date, double Total, double Change) {
        this.Id = Id;
        this.Price = Price;
        this.Discount = Discount;
        this.Receivemoney = Receivemoney;
        this.Date = Date;
        this.Total = Total;
        this.Change = Change;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    public double getDiscount() {
        return Discount;
    }

    public void setDiscount(double Discount) {
        this.Discount = Discount;
    }

    public double getReceivemoney() {
        return Receivemoney;
    }

    public void setReceivemoney(double Receivemoney) {
        this.Receivemoney = Receivemoney;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double Total) {
        this.Total = Total;
    }

    public double getChange() {
        return Change;
    }

    public void setChange(double Change) {
        this.Change = Change;
    }

    @Override
    public String toString() {
        return "Order{" + "Id=" + Id + ", Price=" + Price + ", Discount=" + Discount + ", Receivemoney=" + Receivemoney + ", Date=" + Date + ", Total=" + Total + ", Change=" + Change + '}';
    }
}
