/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author erngeoey
 */
public class Employee {
    private int id;
    private String name;
    private String position;
    private int salary;
    private String email;
    private String sex;
    private String workDate;
    private String tel;
    private String status;

    public Employee(int id, String name, String position, int salary, String email, String sex, String workDate, String tel, String status) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.salary = salary;
        this.email = email;
        this.sex = sex;
        this.workDate = workDate;
        this.tel = tel;
        this.status = status;
    }

    public Employee() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Employee(int id, String name, String position, int salary, String workDate) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.salary = salary;
        this.workDate = workDate;
    }

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getWorkDate() {
        return workDate;
    }

    public void setWorkDate(String workDate) {
        this.workDate = workDate;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", position=" + position + ", salary=" + salary + ", email=" + email + ", sex=" + sex + ", workDate=" + workDate + ", tel=" + tel + ", status=" + status + '}';
    }
    
    

    
    
    
}
