/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.ProductDao;
import java.util.ArrayList;

/**
 *
 * @author Cake
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
    
    
    public Product(int id, String name, double price,String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
 

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}'+"";
    }

    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        ProductDao dao = new ProductDao();
        list = dao.getAll();
//        list.add(dao.get(2));
//        list.add(dao.get(3));
//        list.add(dao.get(5));
//        list.add(dao.get(6));
//        list.add(dao.get(7));
//        list.add(dao.get(8));
//        list.add(dao.get(9));
        return list;
        
    }
   
}
