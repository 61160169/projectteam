/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;
import java.util.ArrayList;

/**
 *
 * @author Triwit
 */
public class Receipt {

    private int id;
    private Date date;
    private User seller;
    private Customer customer;
    private double simlpeTotal;

    public Receipt(int id, Date date, User seller, Customer customer, double simlpeTotal) {
        this.id = id;
        this.date = date;
        this.seller = seller;
        this.customer = customer;
        this.simlpeTotal = simlpeTotal;
    }
    private ArrayList<ReceiptDetail> receiptDetails;

    public Receipt(int id, Date date, User seller, Customer customer) {
        this.id = id;
        this.date = date;
        this.customer = customer;
        this.seller = seller;
        receiptDetails = new ArrayList<>();
    }

    public Receipt(User seller, Customer customer) {
        this(-1, null, seller, customer);
    }


    public void addReceiptDetail(int id, Product product, int amount, double price) {
        for(int i=0;i<receiptDetails.size();i++){
            ReceiptDetail receipt = receiptDetails.get(i);
            if(receipt.getProduct().getName()==product.getName()){
                receipt.TotalAmount(amount);
                return;
            }
        }
        receiptDetails.add(new ReceiptDetail(id, product, amount, price, this));
    }

    public void addReceiptDetail(Product product, int amount) {
        addReceiptDetail(-1, product, amount, product.getPrice());
    }

    public double getSimlpeTotal() {
        return simlpeTotal;
    }

    public void setSimlpeTotal(double simlpeTotal) {
        this.simlpeTotal = simlpeTotal;
    }
    
    
    public void setId(int id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setReceiptDetails(ArrayList<ReceiptDetail> receiptDetails) {
        this.receiptDetails = receiptDetails;
    }

    public double getTotal() {
        double total = 0;
        for (ReceiptDetail t : receiptDetails) {
            total += t.Total();
        }
        return total;
    }
    
    public int getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public User getSeller() {
        return seller;
    }

    public Customer getCustomer() {
        return customer;
    }
    
    public void deleteReceiptDetail(int r){
        receiptDetails.remove(r);
    }
    
    public String showSimple(){
        return "Receipt{" + "id=" + id + ", date=" + date + ", seller=" + seller.getUsername() + ", customer=" + customer.getName() + ", Total="+getSimlpeTotal()+"}";
    }
    @Override
    public String toString() {
        String str = "Receipt{" + "id=" + id + ", date=" + date + ", seller=" + seller.getUsername() + ", customer=" + customer.getName() + ", Total="+getTotal()+"}\n";
        for (ReceiptDetail t : receiptDetails) {
            str = str + t.toString()+ "\n";
        }
        return str;
    }

    public ArrayList<ReceiptDetail> getReceiptDetails() {
        return receiptDetails;
    }

}
