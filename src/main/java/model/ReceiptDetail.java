/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Triwit
 */
public class ReceiptDetail {

    private int id;
    private Product product;
    private int amount;
    private Double price;
    private Receipt receipt;

    public void setId(int id) {
        this.id = id;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public int getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }

    public int getAmount() {
        return amount;
    }

    public Double getPrice() {
        return price;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public ReceiptDetail(int id, Product product, int amount, Double price, Receipt receipt) {
        this.id = id;
        this.product = product;
        this.amount = amount;
        this.price = price;
        this.receipt = receipt;
    }
    
    public void TotalAmount(int amount) {
        this.amount += amount;
    }
    public ReceiptDetail(Product product, int amount, Double price, Receipt receipt) {
        this(-1, product, amount, price, receipt);
    }

    public double Total() {
        return amount * price;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + ", receipt_id=" + getReceipt().getId() + ", product=" + product + ", amount=" + amount + ", price=" + price + ", total "+Total()+'}';
    }
    
    
}
