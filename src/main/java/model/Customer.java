/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Cake
 */
public class Customer {
    private int id;
    private String name;
    private String email;
     private String tel;
    private int point;

    public Customer(int id, String name, String tel) {
        this.id = id;
        this.name = name;
        this.tel = tel;
    }

    public Customer(int id, String name, String email,String tel, int point) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.tel = tel;
        this.point = point;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
         
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
     public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name   + ", email=" + email + ", tel=" + tel + ", point=" + point +'}';
    }
    
}
