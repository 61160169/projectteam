/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NOOM HR
 */
public class PromotionDetail {
    
    private int id;
    private Promotion promotion;
    private Product product;

    public PromotionDetail(int id, Promotion promotion, Product product) {
        this.id = id;
        this.promotion = promotion;
        this.product = product;
    }

    public PromotionDetail(Promotion promotion, Product product) {
        this(-1,promotion,product);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "PromotionDetail{" + "id=" + id + ", promotion=" + promotion + ", product=" + product + '}';
    }
      
}
