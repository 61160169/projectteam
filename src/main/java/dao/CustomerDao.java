/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import crud.TestSelectCustomer;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author Cake
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO customer (name,email,tel,point)VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getEmail());
            stmt.setString(3, object.getTel());
            stmt.setInt(4, object.getPoint());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id, name,email,tel,point FROM customer ;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String email = result.getString("email");
                String tel = result.getString("tel");
                int point = result.getInt("point");
                Customer customer = new Customer(id, name, email,tel,point);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id, name,email,tel,point FROM customer WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int cid = result.getInt("id");
                String name = result.getString("name");
                String email = result.getString("email");
                String tel = result.getString("tel");
                int point = result.getInt("point");
                Customer customer = new Customer(cid, name, tel,email,point);
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
       Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM customer WHERE id = ? ;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

       db.close();
       return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE customer SET name = ?, email = ?, tel = ?, point = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(3, object.getEmail());
            stmt.setString(2, object.getTel());
            stmt.setInt(4, object.getPoint());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return row;
    }

    public static void main(String[] args) {
       CustomerDao cust = new CustomerDao();
        System.out.println(cust.getAll());
        System.out.println(cust.get(2));
        
        cust.delete(4);
        Customer deleteCustomer = cust.get(4);
        System.out.println("delete customer: " + deleteCustomer);


    }
}
