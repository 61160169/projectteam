/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import crud.TestSelectEmployee;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author erngeoey
 */
public class EmployeeDao implements DaoInterface<Employee> {

    @Override
    public int add(Employee emp) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO employee (name,position,salary,email,sex,workdate,tel,status) VALUES (?,?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            //Employee emp = new Employee(-1, "Peach Lee", "Part-Time", 40, "Peach@gmail.com", "Male", "Everyday", "0866652131","working");
            stmt.setString(1, emp.getName());
            stmt.setString(2, emp.getPosition());
            stmt.setDouble(3, emp.getSalary());
            stmt.setString(4, emp.getEmail());
            stmt.setString(5, emp.getSex());
            stmt.setString(6, emp.getWorkDate());
            stmt.setString(7, emp.getTel());
            stmt.setString(8, emp.getStatus());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
            System.out.println("Affect row " + row + " id " + id);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,sex,position,salary,workdate,tel,email,status FROM employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int eid = result.getInt("id");
                String name = result.getString("name");
                String sex = result.getString("sex");
                String position = result.getString("position");
                int salary = result.getInt("salary");
                String workDate = result.getString("workDate");
                String tel = result.getString("tel");
                String email = result.getString("email");
                String status = result.getString("status");
                Employee emp = new Employee(eid, name, position, salary, email, sex, workDate, tel, status);
                list.add(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }
    
    public ArrayList<Employee> getAllWorking() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,sex,position,salary,workdate,tel,email,status FROM employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int eid = result.getInt("id");
                String name = result.getString("name");
                String sex = result.getString("sex");
                String position = result.getString("position");
                int salary = result.getInt("salary");
                String workDate = result.getString("workDate");
                String tel = result.getString("tel");
                String email = result.getString("email");
                String status = result.getString("status");
                Employee emp = new Employee(eid, name, position, salary, email, sex, workDate, tel, status);
                if(emp.getStatus().equals("working")){
                   list.add(emp); 
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,sex,position,salary,workdate,tel,email,status FROM employee WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int eid = result.getInt("id");
                String name = result.getString("name");
                String sex = result.getString("sex");
                String position = result.getString("position");
                int salary = result.getInt("salary");
                String workDate = result.getString("workDate");
                String tel = result.getString("tel");
                String email = result.getString("email");
                String status = result.getString("status");
                Employee emp = new Employee(eid, name, position, salary, email, sex, workDate, tel, status);
                return emp;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM employee WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            //Employee emp = new Employee(3, "Peach Lee", "Part-Time", 40, "Peach@gmail.com", "Male", "Everyday", "0866652131", "working");
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Employee emp) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        
        try {
            String sql = "UPDATE employee SET name = ?, position = ?, salary = ?, email = ?, sex = ?, workdate = ?, tel = ?, status = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            //Employee emp = new Employee(2, "Hana Yoyo", "Part-Time", 400, "Hana@gmail.com","Female","Sat-Sun","0846695156", "working");
            stmt.setString(1, emp.getName());
            stmt.setString(2, emp.getPosition());
            stmt.setDouble(3, emp.getSalary());
            stmt.setString(4, emp.getEmail());
            stmt.setString(5, emp.getSex());
            stmt.setString(6, emp.getWorkDate());
            stmt.setString(7, emp.getTel());
            stmt.setString(8, emp.getStatus());
            stmt.setInt(9, emp.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        return row;
    }

    public static void main(String[] args) {
        EmployeeDao dao = new EmployeeDao();
       
        System.out.println(dao.get(2));
        
       

    }
}
