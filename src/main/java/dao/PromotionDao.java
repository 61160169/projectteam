/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import crud.TestSelectPromotion;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Promotion;

/**
 *
 * @author NOOM HR
 */
public class PromotionDao implements DaoInterface<Promotion> {

    private static int id;

    @Override
    public int add(Promotion pro) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;

        try {
            String sql = "INSERT INTO promotion (name,detail,discount,datestart,datestop)VALUES (?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, pro.getName());
            stmt.setString(2, pro.getDetail());
            stmt.setDouble(3, pro.getDiscount());
            stmt.setString(4, pro.getDatestart());
            stmt.setString(5, pro.getDatestop());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
            System.out.println("Affect row " + row + " id:" + id);

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;

    }

    @Override
    public ArrayList<Promotion> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,detail,discount,datestart,datestop FROM promotion";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String detail = result.getString("detail");
                double discount = result.getDouble("discount");
                String datestart = result.getString("datestart");
                String datestop = result.getString("datestop");
                Promotion pro = new Promotion(id, name, detail, discount, datestart, datestop);
                list.add(pro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Promotion get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,detail,discount,datestart,datestop FROM promotion WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                String detail = result.getString("detail");
                double discount = result.getDouble("discount");
                String datestart = result.getString("datestart");
                String datestop = result.getString("datestop");
                Promotion pro = new Promotion(id, name, detail, discount, datestart, datestop);
                return pro;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM promotion WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;

    }

    @Override
    public int update(Promotion pro) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE promotion SET name = ?, detail = ?, discount = ?, datestart = ?, datestop = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, pro.getName());
            stmt.setString(2, pro.getDetail());
            stmt.setDouble(3, pro.getDiscount());
            stmt.setString(4, pro.getDatestart());
            stmt.setString(5, pro.getDatestop());
            stmt.setInt(6, pro.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;

    }

    

}
