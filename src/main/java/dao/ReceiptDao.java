/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import crud.TestSelectCustomer;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.text.DateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.User;

/**
 *
 * @author Triwit
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt receipt) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO receipt (\n"
                    + "                        cutomer_id,\n"
                    + "                        user_id,\n"
                    + "                        total\n"
                    + "                    )\n"
                    + "                    VALUES (\n"
                    + "                        ?,\n"
                    + "                        ?,\n"
                    + "                       ?\n"
                    + "                    )";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, receipt.getCustomer().getId());
            stmt.setInt(2, receipt.getSeller().getId());
            stmt.setDouble(3, receipt.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                receipt.setId(id);
            }

            for (ReceiptDetail rde : receipt.getReceiptDetails()) {
                String sqlde = "INSERT INTO receipt_detail (\n"
                        + "                               receipt_id,\n"
                        + "                                product_id,\n"
                        + "                               price,\n"
                        + "                               amount\n"
                        + "                           )\n"
                        + "                           VALUES (?,?,?,?)";
                PreparedStatement stmtde = conn.prepareStatement(sqlde);
                stmtde.setInt(1, rde.getReceipt().getId());
                stmtde.setInt(2, rde.getProduct().getId());
                stmtde.setDouble(3, rde.getPrice());
                stmtde.setInt(4, rde.getAmount());

                int rowde = stmtde.executeUpdate();
                ResultSet resultde = stmtde.getGeneratedKeys();
                if (resultde.next()) {
                    id = resultde.getInt(1);
                    rde.setId(id);
                }
            }
            System.out.println("pass");
        } catch (SQLException ex) {
            System.out.println("err");
        }
        db.close();
        return receipt.getId();
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.id as id,\n"
                    + "    created,\n"
                    + "    c.id as customer_id,\n"
                    + "    c.name as customer_name,\n"
                    + "    c.tel as customer_tel,\n"
                    + "    u.id as user_id,\n"
                    + "    u.username as user_name,\n"
                    + "    u.password as user_password, \n"
                    + "    total\n"
                    + "FROM receipt r , customer c , user u \n"
                    + "WHERE r.cutomer_id = c.id AND r.user_id = u.id \n"
                    + "ORDER BY created DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_password");
                double total = result.getDouble("total");
                Receipt receiptsimple = new Receipt(id, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel),
                        total
                );

                Receipt receipt = new Receipt(id, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel)
                );

                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all receipt!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing all receipt!! " + ex.getMessage());
        }

        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.id as id,\n"
                    + "    created,\n"
                    + "    c.id as customer_id,\n"
                    + "    c.name as customer_name,\n"
                    + "    c.tel as customer_tel,\n"
                    + "    u.id as user_id,\n"
                    + "    u.username as user_name,\n"
                    + "    u.password as user_password, \n"
                    + "    total\n"
                    + "FROM receipt r , customer c , user u \n"
                    + "WHERE r.id = ? AND r.cutomer_id = c.id AND r.user_id = u.id";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Date created = new SimpleDateFormat("yyyy-MM-dd").parse(result.getString("created"));
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_password");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(id, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel)
                );
                getReceiptDetail(conn,id,receipt);
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date parsing receipt!! " + ex.getMessage());
        }

        db.close();
        return null;
    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.id as id,\n"
                + "       receipt_id,\n"
                + "       product_id,\n"
                + "       p.name as product_name,\n"
                + "       p.price as product_price,\n"
                + "       rd.price as price,\n"
                + "       amount\n"
                + "  FROM receipt_detail rd, product p\n"
                + "  WHERE receipt_id = ? AND rd.product_id = p.id;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while (resultDetail.next()) {
            int ReceiptId = resultDetail.getInt("id");
            int productId = resultDetail.getInt("product_id");
            String productName = resultDetail.getString("product_name");
            double productPrice = resultDetail.getDouble("product_price");
            double price = resultDetail.getDouble("price");
            int amount = resultDetail.getInt("amount");
            Product product = new Product(productId, productName, productPrice);
            receipt.addReceiptDetail(ReceiptId, product, amount, price);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM receipt WHERE id = ? ;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("finn");

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void main(String[] args) {
        Customer customer = new Customer(1, "Harry Johnson", "0811111111");
        User sell = new User(2, "hana", "hana123");
        Product p1 = new Product(1, "Americano", 40);
        Product p2 = new Product(2, "Esspresso", 40);
        Product p3 = new Product(3, "Thai Tea", 40);
        Receipt r1 = new Receipt(sell, customer);
        r1.addReceiptDetail(p1, 1);
        r1.addReceiptDetail(p2, 2);
        r1.addReceiptDetail(p3, 1);
//        System.out.println(r1);

        ReceiptDao dao = new ReceiptDao();
//        System.out.println("id = " + dao.add(r1));
//        System.out.println("after : " + r1);
        System.out.println("ALL : " + dao.getAll());
        System.out.println("Get 2 = "+dao.get(2));

    }
}
