/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import crud.TestSelectCustomer;
import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.User;

/**
 *
 * @author Triwit
 */
public class LoginDao {
    
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT ID,EMP_ID,USERNAME,PASSWORD FROM USER;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int emp = result.getInt("EMP_ID");
                String Name = result.getString("USERNAME");
                String Password = result.getString("PASSWORD");
                User user = new User(id, emp, Name, Password);
                list.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

 

    public static void main(String[] args) {
        LoginDao dao = new LoginDao();
        System.out.println(dao.getAll());

    }
}
