/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.projectteam;

import dao.EmployeeDao;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import model.Employee;

/**
 *
 * @author erngeoey
 */
public class EmpPanel extends javax.swing.JPanel {

    private final ArrayList<Employee> empList;
    private final EmployeeTableModel model;
    Employee editAddEmp;
    EditEmpFrame frame = new EditEmpFrame();

    /**
     * Creates new form EmpPanel
     */
    public EmpPanel() {
        initComponents();

        EmployeeDao dao = new EmployeeDao();
        empList = dao.getAll();
        model = new EmpPanel.EmployeeTableModel(empList);
        tblEmp.setModel(model);
        initForm();
    }
    
    public void initForm(){
        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblEmp = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();

        setBackground(new java.awt.Color(225, 214, 203));
        setMaximumSize(new java.awt.Dimension(700, 500));
        setMinimumSize(new java.awt.Dimension(700, 500));
        setPreferredSize(new java.awt.Dimension(750, 450));

        tblEmp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No.", "ID", "Name", "Sex", "Position", "Salary", "WorkDate", "Tel.", "Email", "Status"
            }
        ));
        tblEmp.setMaximumSize(new java.awt.Dimension(700, 500));
        tblEmp.setMinimumSize(new java.awt.Dimension(700, 500));
        tblEmp.setPreferredSize(new java.awt.Dimension(700, 500));
        tblEmp.setSelectionBackground(new java.awt.Color(194, 168, 143));
        tblEmp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblEmpMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblEmp);
        if (tblEmp.getColumnModel().getColumnCount() > 0) {
            tblEmp.getColumnModel().getColumn(0).setMinWidth(30);
            tblEmp.getColumnModel().getColumn(0).setPreferredWidth(30);
            tblEmp.getColumnModel().getColumn(0).setMaxWidth(30);
            tblEmp.getColumnModel().getColumn(1).setMinWidth(45);
            tblEmp.getColumnModel().getColumn(1).setPreferredWidth(45);
            tblEmp.getColumnModel().getColumn(1).setMaxWidth(45);
            tblEmp.getColumnModel().getColumn(2).setMinWidth(160);
            tblEmp.getColumnModel().getColumn(2).setPreferredWidth(160);
            tblEmp.getColumnModel().getColumn(2).setMaxWidth(160);
            tblEmp.getColumnModel().getColumn(3).setMinWidth(45);
            tblEmp.getColumnModel().getColumn(3).setPreferredWidth(45);
            tblEmp.getColumnModel().getColumn(3).setMaxWidth(45);
            tblEmp.getColumnModel().getColumn(9).setMinWidth(60);
            tblEmp.getColumnModel().getColumn(9).setPreferredWidth(60);
            tblEmp.getColumnModel().getColumn(9).setMaxWidth(60);
        }

        jPanel1.setBackground(new java.awt.Color(194, 168, 143));

        jLabel1.setBackground(new java.awt.Color(194, 168, 143));
        jLabel1.setFont(new java.awt.Font("Tw Cen MT", 0, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Employee Management");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 730, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(225, 214, 203));

        btnAdd.setBackground(new java.awt.Color(169, 146, 113));
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(169, 146, 113));
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(169, 146, 113));
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnRefresh.setBackground(new java.awt.Color(169, 146, 113));
        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnRefresh)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRefresh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAdd)
                            .addComponent(btnEdit)
                            .addComponent(btnDelete))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 750, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        frame = new EditEmpFrame();
        frame.createEmpNull();
        frame.loadEmpToForm();
        frame.setVisible(true);
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        frame = new EditEmpFrame();
        if (tblEmp.getSelectedRow() >= 0) {
            editAddEmp = empList.get(tblEmp.getSelectedRow());
            frame.createEmp(editAddEmp.getId(), editAddEmp.getName(), editAddEmp.getPosition(), editAddEmp.getSalary(), editAddEmp.getEmail(), editAddEmp.getSex(), editAddEmp.getWorkDate(), editAddEmp.getTel(), editAddEmp.getStatus());
            frame.loadEmpToForm();
        }
        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);
        frame.setVisible(true);
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int reply = JOptionPane.showConfirmDialog(null, "Delete?", "Confirm Delete", JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            if (tblEmp.getSelectedRow() >= 0) {
                EmployeeDao dao = new EmployeeDao();
                editAddEmp = empList.get(tblEmp.getSelectedRow());
                dao.delete(editAddEmp.getId());
            }
        }
        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        EmployeeDao dao = new EmployeeDao();
        ArrayList<Employee> newlist = dao.getAll();
        empList.clear();
        empList.addAll(newlist);
        tblEmp.revalidate();
        tblEmp.repaint();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void tblEmpMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEmpMouseClicked
        if (tblEmp.getSelectedRow() >= 0) {
            btnEdit.setEnabled(true);
            btnDelete.setEnabled(true);
        }
    }//GEN-LAST:event_tblEmpMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblEmp;
    // End of variables declaration//GEN-END:variables

    private class EmployeeTableModel extends AbstractTableModel {

        private final ArrayList<Employee> data;
        String[] columnName = {"id", "Name", "Sex", "Position", "Salary", "Workdate", "Tel", "Email", "Status"};

        public EmployeeTableModel(ArrayList<Employee> data) {
            this.data = data;
        }

        @Override
        public int getRowCount() {
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 9;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Employee emp = this.data.get(rowIndex);

            if (columnIndex == 0) {
                return emp.getId();
            }
            if (columnIndex == 1) {
                return emp.getName();
            }
            if (columnIndex == 2) {
                return emp.getSex();
            }
            if (columnIndex == 3) {
                return emp.getPosition();
            }
            if (columnIndex == 4) {
                return emp.getSalary();
            }
            if (columnIndex == 5) {
                return emp.getWorkDate();
            }
            if (columnIndex == 6) {
                return emp.getTel();
            }
            if (columnIndex == 7) {
                return emp.getEmail();
            }
            if (columnIndex == 8) {
                return emp.getStatus();
            }
            return "";
        }

        public String getColumnName(int column) {
            return columnName[column];
        }
    }
}
