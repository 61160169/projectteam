/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author Cake
 */
public class TestDeleteCustomer {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "DELETE FROM customer WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Customer cust = new Customer(1,"Focus Jeerakul","0981452368","ppingg@gmail.com",2500);
            stmt.setInt(1, cust.getId());
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
}
