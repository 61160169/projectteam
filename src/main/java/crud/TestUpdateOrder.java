/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NOOM HR
 */
public class TestUpdateOrder {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "UPDATE ORDER SET PRICE = ?,DISCOUNT = ?,RECEIVEMONEY = ?,DATE = ?,TOTAL = ?,CHANGE = ? WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, 1000);
            stmt.setDouble(2, 1000);
            stmt.setDouble(3, 1000);
            stmt.setString(4, "05/10/2020");
            stmt.setDouble(5, 1000);
            stmt.setDouble(6, 1000);
            stmt.setInt(7, 6);
            int row = stmt.executeUpdate();

            System.out.println("Affect row " + row);

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectOrder.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }

}
