/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author erngeoey
 */
public class TestDeleteEmployee {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "DELETE FROM employee WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Employee emp = new Employee(3, "Peach Lee", "Part-Time", 40, "Peach@gmail.com", "Male", "Everyday", "0866652131","working");
            stmt.setInt(1, emp.getId());
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
    
}
