/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Promotion;

/**
 *
 * @author NOOM HR
 */
public class TestUpdatePromotion {
    
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "UPDATE promotion SET name = ?, detail = ?, discount = ?, datestart = ?, datestop = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Promotion pro = new Promotion(1,"Happy Birthday","Customers who bought products during the promotion Just show your ID card to get 50% discount.",0.5,"20/10/2020","25/10/2020");
            stmt.setString(1, pro.getName());
            stmt.setString(2, pro.getDetail());
            stmt.setDouble(3, pro.getDiscount());
            stmt.setString(4, pro.getDatestart());
            stmt.setString(5, pro.getDatestop());
            stmt.setInt(6, pro.getId());
            int row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
    }
    
}
