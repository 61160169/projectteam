/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;



/**
 *
 * @author NOOM HR
 */
public class TestInsertOrder {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "INSERT INTO ORDER (PRICE,DISCOUNT,RECEIVEMONEY,DATE,TOTAL,CHANGE)VALUES (?,?,?,?,?,?) ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Order order = new Order(-1,0,0,0,"20/10/2020",0,0);
            stmt.setInt(1, order.getId());
            stmt.setDouble(2, order.getPrice());
            stmt.setDouble(3, order.getDiscount());
            stmt.setDouble(4, order.getReceivemoney());
            stmt.setString(5, order.getDate());
            stmt.setDouble(6, order.getTotal());
            stmt.setDouble(7, order.getChange());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if (result.next()) {
                id = result.getInt(1);
            }
            System.out.println("Affect row " + row + " ID:" + id);

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
