/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;



import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;


/**
 *
 * @author DELL
 */
public class TestSelectUser {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();      
        try {
            String sql = "SELECT id,emp_id,username,password FROM user";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
              
            while (result.next()) {
                int id = result.getInt("id");  
                int emp_id =  result.getInt("emp_id"); 
                String username = result.getString("username");
                String password = result.getString("password");
         
          
                User user = new User(id,emp_id,username,password);
                System.out.println(user);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getTypeName()).log(Level.SEVERE, null, ex);
        }

      db.close();  
    }

}

