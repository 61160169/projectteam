/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author Triwit
 */
public class TestReceipt {

    public static void main(String[] triwit) {
        Customer customer = new Customer(0, "yanhhi", "00000000", "triwit24", 0);
        User sell = new User(0, "but01", "1234");
        Product p1 = new Product(1, "gafair", 50, "");
        Product p2 = new Product(2, "gadum", 60, "");
        Product p3 = new Product(1, "cha", 30, "");
        Receipt r1 = new Receipt(sell, customer);
        r1.addReceiptDetail(p1, 2);
        r1.addReceiptDetail(p2, 4);
        r1.addReceiptDetail(p1, 1);
        r1.addReceiptDetail(p3, 1);
        System.out.println(r1);
    }

}
