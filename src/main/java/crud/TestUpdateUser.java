/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class TestUpdateUser {
      public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "UPDATE user SET username = ?,password = ? WHERE  id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);         
            stmt.setString(1,"popole");
            stmt.setString(2,"88888888");
            stmt.setInt(3,4);
            int row = stmt.executeUpdate();       
            System.out.println("Affect row "+ row );
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }

}
