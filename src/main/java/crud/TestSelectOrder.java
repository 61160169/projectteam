/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;


/**
 *
 * @author NOOM HR
 */
public class TestSelectOrder {
    public static void main(String[] args){
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,price,discount,receivemoney,date,total,change FROM order;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                
                int Id = result.getInt("id");
                double Price = result.getDouble("price");
                double Discount = result.getDouble("discount");
                double Receivemoney = result.getDouble("receivemoney");
                String Date = result.getString("date");
                double Total = result.getDouble("total");
                double Change = result.getDouble("change");
                
                Order order = new Order(Id,Price,Discount,Receivemoney,Date,Total,Change);
                System.out.println(order);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }

}
