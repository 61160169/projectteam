/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Stock;

/**
 *
 * @author User
 */
public class TestSelectStock {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,amount,expdate FROM stock";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
              
            while (result.next()) {
                int id = result.getInt("id");  
                String name = result.getString("name");
                int amount = result.getInt("amount");
                String date = result.getString("expdate");
         
          
                Stock stock = new Stock(id,name,amount,date);
                System.out.println(stock);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectStock.class.getTypeName()).log(Level.SEVERE, null, ex);
        }

      db.close();  
    } 
    
         
}
