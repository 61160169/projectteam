/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
/**
 *
 * @author Cake
 */
public class TestUpdateCustomer {
    public static void main(String[] args) {
Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "UPDATE customer SET name = ?,tel = ? ,email = ?,point = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,"Nat Chalee");
            stmt.setString(2,"0941234567");
            stmt.setString(3, "nattt@gmail.com");
            stmt.setInt(4, 1250);
            stmt.setInt(5,2);
            int row = stmt.executeUpdate();
           
            System.out.println("Affect row "+ row );
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
}
