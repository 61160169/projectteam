/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author Cake
 */
public class TestSelectCustomer {
    public static void main(String [] args){
    Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();      
        try {
            String sql = "SELECT id,name,tel,email,point FROM customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
              
            while (result.next()) {
                int id = result.getInt("id");  
                String name =  result.getString("name"); 
                String tel = result.getString("tel");
                String email = result.getString("email");
                int point = result.getInt("point");
              
         
          
                Customer cust = new Customer(id,name,tel,email,point);
                System.out.println(cust);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getTypeName()).log(Level.SEVERE, null, ex);
        }

      db.close();  
}
}
