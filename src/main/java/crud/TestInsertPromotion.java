/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Promotion;

/**
 *
 * @author NOOM HR
 */
public class TestInsertPromotion {
    
    public static void main(String []args){
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try{
            String sql = "INSERT INTO promotion (name,detail,discount,datestart,datestop)VALUES (?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Promotion pro = new Promotion(-1,"Best Seller","Buy products that define the car immediately 50%",0.5,"01/10/2020","01/12/2020");
            stmt.setString(1,pro.getName());
            stmt.setString(2,pro.getDetail());
            stmt.setDouble(3,pro.getDiscount());
            stmt.setString(4,pro.getDatestart());
            stmt.setString(5,pro.getDatestop());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if (result.next()) {
                id = result.getInt(1);
            }
            System.out.println("Affect row " + row + " id:" + id);

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }

       db.close();
            
    }
    
}
