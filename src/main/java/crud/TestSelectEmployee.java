/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author erngeoey
 */
public class TestSelectEmployee {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "SELECT id,name,sex,position,salary,workdate,tel,email,status FROM employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                String sex = result.getString("sex");
                String position = result.getString("position");
                int salary = result.getInt("salary");
                String workDate = result.getString("workDate");
                String tel = result.getString("tel");
                String email = result.getString("email");
                String status = result.getString("status");
                Employee emp = new Employee(id,name,position,salary,email,sex,workDate,tel,status);
                System.out.println(emp);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
    
}
