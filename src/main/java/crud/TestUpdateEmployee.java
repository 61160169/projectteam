/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author erngeoey
 */
public class TestUpdateEmployee {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "UPDATE employee SET name = ?, position = ?, salary = ?, email = ?, sex = ?, workdate = ?, tel = ?, status = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Employee emp = new Employee(2, "Hana Yoyo", "Part-Time", 400, "Hana@gmail.com","Female","Sat-Sun","0846695156", "working");
            stmt.setString(1, emp.getName());
            stmt.setString(2, emp.getPosition());
            stmt.setDouble(3, emp.getSalary());
            stmt.setString(4, emp.getEmail());
            stmt.setString(5, emp.getSex());
            stmt.setString(6, emp.getWorkDate());
            stmt.setString(7, emp.getTel());
            stmt.setString(8, emp.getStatus());
            stmt.setInt(9, emp.getId());
            int row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
    }
    
}
