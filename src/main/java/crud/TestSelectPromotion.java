/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Promotion;

/**
 *
 * @author NOOM HR
 */
public class TestSelectPromotion {
    
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "SELECT id,name,detail,discount,datestart,datestop FROM promotion";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                String detail = result.getString("detail");
                double discount = result.getDouble("discount");
                String datestart = result.getString("datestart");
                String datestop = result.getString("datestop");
                Promotion pro = new Promotion(id,name,detail,discount,datestart,datestop);
                System.out.println(pro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
    
}
