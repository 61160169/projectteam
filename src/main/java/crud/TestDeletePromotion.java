/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Promotion;

/**
 *
 * @author NOOM HR
 */
public class TestDeletePromotion {
    
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "DELETE FROM promotion WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Promotion pro = new Promotion(4,"Best Seller","Buy products that define the car immediately 50%",0.5,"01/10/2020","01/12/2020");
            stmt.setInt(1,pro.getId());
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectPromotion.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
    
}
